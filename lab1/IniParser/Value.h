//
// Created by dk on 09.09.17.
//

#ifndef LAB1_VALUE_H
#define LAB1_VALUE_H

#include <string>
#include <iostream>
#include "ValueType.h"

using namespace std;

class Value {
private:
    ValueType type;
    string strVal;
    double doubleVal;
    int intVal;
public:
    Value(string val);

    int getInt();

    double getDouble();

    string &getString();

};


#endif //LAB1_VALUE_H
