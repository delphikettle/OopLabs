//
// Created by dk on 09.09.17.
//

#ifndef LAB1_INIPARSER_H
#define LAB1_INIPARSER_H

#include <string>
#include <map>
#include "Value.h"


using namespace std;

class IniParser {
private:
    string curSection;
    map<string, map<string, Value *>> sections;

public:
    void parse(string fileName);

    Value &getValue(const char *section, const char *parName);

    void printAll();

    IniParser();

    ~IniParser();

private:
    void parseLine(string &line);

    string parName, parVal;
};


#endif //LAB1_INIPARSER_H
