//
// Created by dk on 09.09.17.
//

#include "Value.h"
#include "../Exceptions/InvalidTypeException.h"


Value::Value(string val) {
    this->strVal = string(val);
    this->type = STRING;

    try {
        this->doubleVal = stod(val);
        this->type = DOUBLE;
    } catch (invalid_argument e) {

    }

    try {
        this->intVal = stoi(val);
        this->type = INT;
    } catch (invalid_argument e) {

    }
}

int Value::getInt() {
    if (this->type == INT)
        return this->intVal;
    else
        throw InvalidTypeException("INT");
}

double Value::getDouble() {
    if (this->type == INT or this->type == DOUBLE)
        return this->doubleVal;
    else
        throw InvalidTypeException("DOUBLE");
}

string &Value::getString() {
    return this->strVal;
}
