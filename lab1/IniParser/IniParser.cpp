//
// Created by dk on 09.09.17.
//

#include <fstream>
#include <regex>
#include "IniParser.h"
#include "../Exceptions/InvalidFilenameException.h"
#include "../Exceptions/InvalidSectionNameException.h"
#include "../Exceptions/InvalidParameterNameException.h"

IniParser::IniParser() = default;

void IniParser::parse(string fileName) {
    ifstream inputFile(fileName);
    if (!inputFile.good())
        throw InvalidFilenameException("Can't open " + fileName);
    string line;
    while (getline(inputFile, line)) {
        parseLine(line);
    }
    inputFile.close();
}

void IniParser::parseLine(string &line) {
    typedef match_results<const char *> cmatch;
    cmatch res;
    regex reg(R"((?: )*(?:\[([\d\w]+)\])?(?: )*(?:([\d\w]+)(?: )*=(?: )*([\S]+)(?: )*)?(?:;.*)?)");
    regex_search(line.c_str(), res, reg);
    if (res[1] != "")
        this->curSection = res[1];
    if (res[2] != "") {
        parName = res[2];
        parVal = res[3];
        Value *val = new Value(parVal);
        this->sections[curSection][parName] = val;
    }
}

Value &IniParser::getValue(const char *section, const char *parName) {
    if (!this->sections.count(section))
        throw InvalidSectionNameException(section);
    if (!this->sections[section].count(parName))
        throw InvalidParameterNameException(parName);
    return *(this->sections[section][parName]);
}

IniParser::~IniParser() {
    for (pair<string, map<string, Value *>> section:this->sections)
        for (pair<string, Value *> val: section.second)
            delete val.second;
}

void IniParser::printAll() {
    for (pair<string, map<string, Value *>> section:this->sections) {
        cout << "Section \"" + section.first + "\"" << endl;
        for (pair<string, Value *> val: section.second) {
            cout << "    " << val.first << " = " << val.second->getString() << endl;
        }
    }

}
