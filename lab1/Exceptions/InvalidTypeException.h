//
// Created by dk on 09.09.17.
//

#ifndef LAB1_INVALIDTYPEEXCEPTION_H
#define LAB1_INVALIDTYPEEXCEPTION_H

#include <stdexcept>
#include <exception>

using namespace std;

class InvalidTypeException : invalid_argument {
public:
    InvalidTypeException(const string &__arg);

};


#endif //LAB1_INVALIDTYPEEXCEPTION_H
