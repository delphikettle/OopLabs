//
// Created by dk on 09.09.17.
//

#include "InvalidTypeException.h"

InvalidTypeException::InvalidTypeException(const string &__arg) : invalid_argument("Invalid type, expected " + __arg) {}
