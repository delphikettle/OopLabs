//
// Created by dk on 09.09.17.
//

#include "InvalidSectionNameException.h"

InvalidSectionNameException::InvalidSectionNameException(const string &__arg) : invalid_argument(
        "Can't find \'" + __arg + "\" section") {}
